<?php

namespace App\Http\Controllers\Bot;

use App\Models\User;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $items = User::all();
        return view('crud.posts.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new User();
        $userList = User::all();

        return view('crud.posts.edit',
        compact('item', 'userList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $item = new User($data);
        $item->save();
        return redirect()
            ->route('crud.posts.index', $item->id)
            ->with(['success'=>'save']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        $userList = User::all();
        return view('crud.posts.edit',
            compact('item', $userList));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::find($id);
        if(empty($item)) {
            return back()
                ->withErrors(['msg'=>"record id=[{$id}] not found"])
                ->withInput();
        }
        $data = $request->all();
        $result = $item->fill($data)->save();

        if ($result){
            return redirect()
                ->route('crud.posts.index', $item->id)
            ->with(['success'=>'save']);
        }else{
            return back()
                ->withErrors(['msg'=>'error'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = User::destroy($id);
        if ($result) {
        return redirect()
            ->route( 'crud.posts.index');
        }else{
            return back()->withErrors(['msg'=>'error']);
        }
    }
}
