<nav><a href="{{ route ('crud.posts.create') }}">CREATE</a></nav>
<table>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
            <td>
                <a href="{{ route ('crud.posts.edit', $item->id) }}">edit</a>
                <a href="{{ route ('crud.posts.destroy', $item->id) }}">delete</a>
                <form method="post" action="{{ route ('crud.posts.destroy', $item->id )}}">
{{--                    <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{route('admin.historie.destroy', $historie)}}" method="post">--}}
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </form>
            </td>
        </tr>
    @endforeach
</table>
