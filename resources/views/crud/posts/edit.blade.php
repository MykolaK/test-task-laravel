@if($item->exists)
        <form method="post" action="{{ route ('crud.posts.update', $item->id )}}">
        @method('PATCH')
    @else
        <form method="post" action="{{ route ('crud.posts.store')}}">
    @endif
    @csrf
    <div>
        <div>
            @include('crud.posts.include.main')
        </div>
        <div>
            <button type="submit">SAVE</button>
        </div>
    </div>
</form>
